import argparse
import json
import os
import time


# Code to initiate/encode/decode objects using different encoders
ENCODERS = {
    "pyfrpc": (
        "import pyfrpc",
        "lambda data: pyfrpc.encode(pyfrpc.FrpcResponse(data), 0x0201)",
        "lambda data: pyfrpc.decode(data).data"
    ),
    "fastrpc": (
        "import fastrpc",
        "lambda data: fastrpc.dumps((data,), useBinary=True)",
        "lambda data: fastrpc.loads(data, useBinary=True)[0]"
    ),
    "pyfastrpc": (
        "import pyfastrpc",
        "lambda data: pyfastrpc.dumps((data,), useBinary=True)",
        "lambda data: pyfastrpc.loads(data, useBinary=True)[0]"
    ),
    "json": ("", "json.dumps", "json.loads"),
    "ujson": ("import ujson", "ujson.dumps", "ujson.loads"),
}


def print_timing(t1, t2):
    print("{:5.1f}ms + {:5.1f}ms = {:5.1f}ms".format(t1, t2, t1 + t2))

def print_label(label):
    print("=" * 50 + "\n " + label + "\n" + "=" * 50 + "\n")


def benchmark_all(repeats_cnt):
    results = []

    for encoder in sorted(ENCODERS.keys()):
        r = benchmark(encoder, repeats_cnt)
        r = [encoder] + list(r[-1])
        results.append(r)

    results.sort(key=lambda r: r[1] + r[2])

    print_label("Summary:")

    for r in results:
        print("{:10} {:5.1f}ms + {:5.1f}ms = {:5.1f}ms".format(
            r[0], r[1], r[2], r[1] + r[2]))

    print()


def benchmark(encoder_name, repeats_cnt):
    encoder = ENCODERS[encoder_name]

    exec(encoder[0], globals())
    encode = eval(encoder[1])
    decode = eval(encoder[2])

    print_label(encoder_name)
    print("encode / decode benchmark:")

    here = os.path.dirname(os.path.realpath(__file__))

    with open(os.path.join(here, 'data.json'), 'r') as f:
        data = json.load(f)
        data *= 10

    timing = []

    for _ in range(repeats_cnt):
        t = time.time()
        x = encode(data)
        t1 = (time.time() - t) * 1000.0

        t = time.time()
        y = decode(x)
        t2 = (time.time() - t) * 1000.0

        timing.append((t1, t2))
        print_timing(t1, t2)

    t1 = sorted(r[0] for r in timing)[2]
    t2 = sorted(r[1] for r in timing)[2]
    timing.append((t1, t2))

    print("\n3rd best result:")
    print_timing(t1, t2)
    print()

    return timing


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-e", choices=list(ENCODERS.keys()) + ["all"],
        default="pyfrpc", help="encoder to benchmark")
    parser.add_argument("-n", type=int, default=20, help="number of repeats")

    args = parser.parse_args()

    if args.e == "all":
        benchmark_all(args.n)
    else:
        benchmark(args.e, args.n)


if __name__ == "__main__":
    main()
