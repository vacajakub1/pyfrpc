# Pyfrpc

Pyfrpc is a Python implementation of FastRPC protocol/library (
[@github](https://seznam.github.io/frpc/) or
[@sourceforge](http://fastrpc.sourceforge.net/)
). The original library contains Python bindings, but:

1. I prefer python packages without dependencies on debian packages.
2. Its flexibility is limited in some points. E.g. the library provides its own 
implementation of HTTP server and client, which is nice to have in C++, but not 
in Python I would say as there are already full-featured and more mature 
libraries available for those tasks.
3. In my opinion it's not worth to wrap such a library for python anyway (at 
least not from today's perspective). The original binding itself is 3500 lines 
of code. So this is an alternative aproach.
4. And I want to use it as a showcase for cython as well.

This library provides FastRPC encoding/decoding functions, FastRPC client 
(based on 'requests') and an interactive FastRPC client/terminal 'pyfrpc' (an 
alternative to 'fastrpc-netcat' based on 'IPython'). API mostly differs and is 
not meant to be a drop-in replacement.

It is compatible with protocol versions 1.0, 2.0, 2.1 and 3.0.

Package provides both cython (C extencion) and pure python implementations. 
Cython implementation is faster but requires C toolchain (or available python 
wheels). To use pure python implementation set env variable `PYFRPC_NOEXT=1` 
during installation.

Some features missing are:

- handling of base64 encoded FastRPC
- helper to build a server


## Interactive client

Interactive client opens na ipython session with varible `client` initialized to 
FastFRPC connection. It supports tab-based completion of remote procedures' 
names as well as shownig their documentation using question mark notation as is 
usual in ipython.

```
$ pyfrpc --help

usage: pyfrpc [-h] [--insecure] URL

FRPC netcat for interactive connection to FRPC server

positional arguments:
  URL         URL of FRPC server to connect to

optional arguments:
  -h, --help  show this help message and exit
  --insecure  Do not check server's cert
```

```
$ pyfrpc http://my-avesome-server

In [1]: client.greeting?
...
Docstring:   Prints greeting.

In [2]: client.greeting()
Out[2]: 
'hello from server'
```


## Benchmark

2020-12-02, Python3.7 @ debian buster:
```
+------------------------+----------+----------+----------+
| description            | encoding | decoding | total    |
+------------------------+----------+----------+----------+
| pyfrpc                 |    1.6ms |    3.6ms |    5.3ms |
| ujson                  |    5.1ms |    4.7ms |    9.9ms |
| fastrpc                |    3.2ms |    7.0ms |   10.2ms |
| json                   |    7.0ms |    5.2ms |   12.3ms |
| pyfastrpc              |   59.2ms |   77.2ms |  136.3ms |
+------------------------+----------+----------+----------+
```

2017-12-29, benchmarks during library development:
```
+------------------------+----------+----------+----------+
| description            | encoding | decoding | total    |
+========================+==========+==========+==========+
| fastrpc (original lib) |   1.7 ms |   5.7 ms |   7.5 ms |
+------------------------+----------+----------+----------+
| python                 |  35.1 ms |  25.8 ms |  60.8 ms |
+------------------------+----------+----------+----------+
| pypy                   |  12.6 ms |   5.2 ms |  17.8 ms |
+------------------------+----------+----------+----------+
| cython (naive)         |  17.9 ms |  43.1 ms |  61.0 ms |
| cython (cdef)          |  11.9 ms |  22.6 ms |  34.4 ms |
| cython (static typing) |   2.2 ms |   3.7 ms |   5.9 ms |
| cython (low level api) |   1.3 ms |   3.7 ms |   4.9 ms |
+------------------------+----------+----------+----------+
```
